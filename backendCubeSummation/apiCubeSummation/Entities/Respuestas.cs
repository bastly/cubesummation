﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Entities
{
    public class Respuestas
    {
        public string response { get; set; }
        public string status { get; set; }
    }
}
