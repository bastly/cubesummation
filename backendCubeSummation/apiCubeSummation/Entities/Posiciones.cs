﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Entities
{
    public class Posiciones
    {
        public static List<int> APosiciones(List<string> valores)
        {
            List<int> posiciones = new List<int>(3);

            for (int i = 0; i < 3; i++)
            {
                posiciones.Add(int.Parse(valores[i]));
            }

            return posiciones;
        }
    }
}
