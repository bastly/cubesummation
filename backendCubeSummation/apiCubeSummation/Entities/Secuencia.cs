﻿using System.Collections.Generic;

namespace apiCubeSummation.Entities
{
    public class Secuencia
    {
        public List<string> Valores { get; set; }
    }
}
