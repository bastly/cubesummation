﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using apiCubeSummation.Entities;
using apiCubeSummation.Models;

namespace apiCubeSummation.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoreController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        [HttpPost]
        [Route("init")]
        public ActionResult<Respuestas> Post(Procesos proceso)
        {
            Respuestas res = new Respuestas();
            try
            {
                Secuencia secuencia = new Secuencia();
                CoreModels core = new CoreModels();
                secuencia.Valores = proceso.ProcesoEntrada.Replace("@", ";").Split(';').ToList();

                List<string> listaMensajesValidacion = core.ValidacionDatosEntrada(secuencia);

                if (listaMensajesValidacion.Count > 0)
                {
                    res.response = string.Join("; ", listaMensajesValidacion);
                    return res;
                }

                List<string> resultadoOperacion = core.IniciarCubeSummationTest(secuencia);

                proceso.ProcesoSalida = string.Join("\r\n", resultadoOperacion);

                res.response = string.Join("\r\n", resultadoOperacion);
                res.status = "200";
                return res;
            }
            catch
            {
                res.response = "datos de entrada incorrectos, por favor valida y vuelve a intentar";
                return res;


            }
        }
    }
}
