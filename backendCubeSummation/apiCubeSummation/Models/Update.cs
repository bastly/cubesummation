﻿using apiCubeSummation.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Models
{
    public class Update : IRoute
    {

        public void EjecutarRoute(List<string> informacion, Matriz matriz, List<string> resultado)
        {
            List<int> pos = Posiciones.APosiciones(informacion.GetRange(1, 3));
            matriz.matriz[pos[0], pos[1], pos[2]] = int.Parse(informacion[4]);
        }
    }
}
