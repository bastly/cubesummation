﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using apiCubeSummation.Entities;

namespace apiCubeSummation.Models
{
    public class CoreModels
    {
        internal List<string> ValidacionDatosEntrada(Secuencia secuencia)
        {
            List<string> listaMensajesValidacion = new List<string>();

            int tamañoMatriz = 0;

            for (int i = 0; i < secuencia.Valores.Count; i++)
            {
                var regexNumeroCasos = new Regex(@"^[0-9]{1,2}$");

                if (regexNumeroCasos.IsMatch(secuencia.Valores[i]))
                {
                    if (int.Parse(secuencia.Valores[i]) < 0 || int.Parse(secuencia.Valores[i]) > 50)
                    {
                        listaMensajesValidacion.Add("El número de casos debe estar entre 1 y 50");
                    }
                }

                var regexTamañoMatrizNumeroOperaciones = new Regex(@"^[0-9]{1,3}\s[0-9]{1,3}$");

                if (regexTamañoMatrizNumeroOperaciones.IsMatch(secuencia.Valores[i]))
                {
                    tamañoMatriz = int.Parse(secuencia.Valores[i].Split(' ').ToList()[0]);
                    int numeroOperaciones = int.Parse(secuencia.Valores[i].Split(' ').ToList()[1]);

                    if (tamañoMatriz < 0 || tamañoMatriz > 100)
                    {
                        listaMensajesValidacion.Add("El tamaño de la matriz debe estar entre 1 y 100");
                    }

                    if (1 < 0 || numeroOperaciones > 1000)
                    {
                        listaMensajesValidacion.Add("El número de operaciones debe estar entre 1 y 1000");
                    }
                }

                if (secuencia.Valores[i].Contains("QUERY"))
                {
                    int coordenadaX1 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[1]);
                    int coordenadaY1 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[2]);
                    int coordenadaZ1 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[3]);

                    int coordenadaX2 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[4]);
                    int coordenadaY2 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[5]);
                    int coordenadaZ2 = int.Parse(secuencia.Valores[i].Split(' ').ToList()[6]);

                    if ((coordenadaX1 < 0 || coordenadaX1 > tamañoMatriz) || (coordenadaX2 < 0 || coordenadaX2 > tamañoMatriz) || (coordenadaY1 < 0 || coordenadaY1 > tamañoMatriz) || (coordenadaY2 < 0 || coordenadaY2 > tamañoMatriz) || (coordenadaZ1 < 0 || coordenadaZ1 > tamañoMatriz) || (coordenadaZ2 < 0 || coordenadaZ2 > tamañoMatriz))
                    {
                        listaMensajesValidacion.Add("El valor de cada cordenada de la operación QUERY debe estar entre 1 y " + tamañoMatriz.ToString());
                    }
                }

                if (secuencia.Valores[i].Contains("UPDATE"))
                {
                    int coordenadaX = int.Parse(secuencia.Valores[i].Split(' ').ToList()[1]);
                    int coordenadaY = int.Parse(secuencia.Valores[i].Split(' ').ToList()[2]);
                    int coordenadaZ = int.Parse(secuencia.Valores[i].Split(' ').ToList()[3]);

                    int valorCoordenadaW = int.Parse(secuencia.Valores[i].Split(' ').ToList()[4]);

                    if ((coordenadaX < 0 || coordenadaX > tamañoMatriz) || (coordenadaY < 0 || coordenadaY > tamañoMatriz) || (coordenadaZ < 0 || coordenadaZ > tamañoMatriz))
                    {
                        listaMensajesValidacion.Add("El valor de cada cordenada de la operación UPDATE debe estar entre 1 y " + tamañoMatriz.ToString());
                    }

                    if (valorCoordenadaW < -109 || valorCoordenadaW > 109)
                    {
                        listaMensajesValidacion.Add("El valor de la coordenada debe estar entre -109 y 109");
                    }
                }

            }
            return listaMensajesValidacion;
        }

        internal List<string> IniciarCubeSummationTest(Secuencia secuencia)
        {
            List<string> resultadoOperacion = new List<string>();

            int posicionEntradas = 0;

            int numeroCasos = int.Parse(secuencia.Valores[posicionEntradas]);
            
            posicionEntradas++;

            for (int i = 0; i < numeroCasos; i++)
            {
                List<string> informacionCaso = secuencia.Valores[posicionEntradas].Split(' ').ToList();

                int tamañoMatriz = int.Parse(informacionCaso[0]) + 1;
                int numeroOperaciones = int.Parse(informacionCaso[1]);

                posicionEntradas++;

                Matriz matriz = new Matriz(tamañoMatriz);

                for (int j = 0; j < numeroOperaciones; j++)
                {
                    List<string> contenidoOperacion = secuencia.Valores[posicionEntradas].Split(' ').ToList();

                    Route route = new Route();

                    route.SRoute(contenidoOperacion);
                    route.ERoute(contenidoOperacion, matriz, resultadoOperacion);

                    posicionEntradas++;

                }

            }

            System.Diagnostics.Debug.WriteLine(string.Join("\r\n", resultadoOperacion));

            return resultadoOperacion;
        }
    }
}
