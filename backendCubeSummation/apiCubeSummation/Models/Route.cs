﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Models
{
    public class Route
    {
        private IRoute route;

        private void AsignarOperacion(IRoute route)
        {
            this.route = route;
        }

        public void SRoute(List<string> informacionOperacion)
        {

            switch (informacionOperacion[0])
            {
                case "UPDATE":
                    AsignarOperacion(new Update());
                    break;

                case "QUERY":
                    AsignarOperacion(new Query());
                    break;
            }
        }

        public void ERoute(List<string> informacion, Matriz matriz, List<string> resultado)
        {
            this.route.EjecutarRoute(informacion, matriz, resultado);
        }
    }
}
