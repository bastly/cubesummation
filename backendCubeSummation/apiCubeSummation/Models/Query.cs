﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apiCubeSummation.Entities;
namespace apiCubeSummation.Models
{
    public class Query: IRoute
    {
        public void EjecutarRoute(List<string> informacion, Matriz matriz, List<string> resultado)
        {
            List<int> cordenadasInicio = Posiciones.APosiciones(informacion.GetRange(1, 3));
            List<int> cordenadasFinales = Posiciones.APosiciones(informacion.GetRange(4, 3));

            int suma = 0;

            for (int x = cordenadasInicio[0]; x <= cordenadasFinales[0]; x++)
            {
                for (int y = cordenadasInicio[1]; y <= cordenadasFinales[1]; y++)
                {
                    for (int z = cordenadasInicio[2]; z <= cordenadasFinales[2]; z++)
                    {
                        suma += matriz.matriz[x, y, z];
                    }
                }

            }

            resultado.Add(suma.ToString());
        }
    }
}
