﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Models
{
    public class Matriz
    {
       
        public int size;
        public int[,,] matriz;
        public Matriz(int size)
        {
            this.size = size;
            ConstruirMatriz();

        }
        private void ConstruirMatriz()
        {
            matriz = new int[size, size, size];
        }

    }
}
