﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiCubeSummation.Models
{
    
    public interface IRoute
    {
        void EjecutarRoute(List<string> informacion, Matriz matriz, List<string> resultado);
    }
    
}
