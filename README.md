Capas de la aplicación.

1. Presentación(View)
frontendCubeSummation/src/app/
app.component.html, app.component.ts, app.component.css

2. Servicio(Controller)
backendCubeSummation
CoreController.cs

3· Negocio(Models)
CoreModels.cs (Su responsabilidad es validar los datos de entrada e iniciar el test llamando a las clases respectivas para la operación)

Update.cs (Su responsabiliad es realizar la operación Update de los datos de entrada del usuario.)

Query.cs (Su responsabiliad es realizar la operación Query de los datos de entrada del usuario.)

Route.cs (Su responsabiliad es rootear las operación ingresadas y dirigirlas para asi llamar a las clases QUERY o UPDATE de acuerdo a lo que ingrese el usuario.)

Matriz.cs (Su responsabiliad es definir una matriz 3D.)

IRoute.cs (Interfase que defina la operación base que van a contener las operaciones QUERY y UPDATE y nos ayuda a implementar el patron STATE)


¿En qué consiste el principio de responsabilidad única?

Consiste en que cada clase, modulo o función tengan una unica razón, tengan una sola labor, un solo fin, y la meta final de este principio es ayudarnos con la cohesion de nuestro código para lograr el código sea facilmente mantenible.

¿Qué es código limpio?

El codigo limpio es tener una estructura bien definidad, tener un buen nombramiento en las clases, metedos, variables, etc.
Que el codigo sea legible y entendible para poderlo mantener.
Que contengan pruebas que validen el funcionamiento del código.
Que en el desarrollo se hayan tenido en cuenta buenos patrones de diseño.