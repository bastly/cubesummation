import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ServiceConfig {
    configUrl = 'http://localhost:49360/api/core/';

    constructor(private http: HttpClient) { }

    setSizeMatriz(data) {
        if (data.ProcesoEntrada) {
            data.ProcesoEntrada = data.ProcesoEntrada.replace(/\n/ig, '@');
        }
        const headers = new HttpHeaders().set('Content-Type', 'application/json;');
        return this.http.post(this.configUrl + 'init', JSON.stringify(data), {headers});
    }
}