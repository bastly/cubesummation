import { Component } from '@angular/core';
import { ServiceConfig } from './service/service.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'CubeSummation';
  core = {
    ProcesoEntrada : '',
    ProcesoSalida : '',
    procesando: false
  };

  constructor(private serviceConfig: ServiceConfig) { }

  setSizeMatriz() {
    this.core.procesando = true;
    this.serviceConfig.setSizeMatriz(this.core)
      .subscribe((data) => {
        this.core.procesando = false;
        this.core.ProcesoSalida = data["response"];
        console.log(data);
      });
  }
}
